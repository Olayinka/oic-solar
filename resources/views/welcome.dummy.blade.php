<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <meta name="keywords" content="OICSolar Solutions">
      <meta name="description" content="We provide your Solar solutions and products">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <meta name="author" content="NanoTech Team">
      
    <!-- reponsive meta-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OICSolar Solutions | Homepage</title>
    <!-- Bootstrap-->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- animate css-->
    <link rel="stylesheet" href="{{asset('css/animate.min.css')}}">
    <!-- owl-carousel-->
    <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('css/settings.css')}}">
    <link rel="stylesheet" href="{{asset('css/layers.css')}}">
    <link rel="stylesheet" href="{{asset('css/navigation.css')}}">
    <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
    <!-- Main Css-->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries-->
    <!-- WARNING: Respond.js doesn't work if you view the page via file://-->
    <!--if lt IE 9
    script(src='https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js')
    script(src='https://oss.maxcdn.com/respond/1.4.2/respond.min.js')
    -->
  </head>
  <body>
    <header id="header">
      <!-- Header Gray Band-->
      <section class="wel-t-band">
        <div class="container">
          <div class="row wel-band-bg">
            <div class="col-md-6 pull-left col col-sm-12">
              <p>Welcome to Leader in Solar Solution <span>since 2009</span></p>
            </div>
            <div class="col-md-6 pull-right text-right col-sm-12">
              <p>RC NO: <span>814913</span></p>
            </div>
          </div>
        </div>
      </section>
      <!-- Header Gray Band-->
      <!-- Logo Section-->
      <div class="container">
        <div class="row pad-logo logo-wrapper">
          <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12 t-logo"><a href="index.html"><img src="{{asset('images/logo.jpg')}}" alt="logo" class="img-responsive"></a></div>
          <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 text-right">
            <div class="top-two-right">
              <div class="req-button text-right"><a href="request-qoute.html" class="submit">requet a quote</a></div>
              <div class="top-panel">
                <div class="touch_top touch_top_pad">
                  <ul class="nav">
                    <li class="item item-phone">
                      <div class="media">
                        <div class="blue-color media-left"><a href="#"><i class="icon icon-Phone2"></i></a></div>
                        <div class="media-body">
                          <p>+ (234) 810 502 0388<br><span><a href="mailto:info@industrial.com">info@industrial.com</a></span></p>
                        </div>
                      </div>
                    </li>
                    <li class="item item-ad">
                      <div class="media">
                        <div class="blue-color media-left"><a href="#"><i class="icon icon-Pointer"></i></a></div>
                        <div class="media-body">
                          <p>13005 Greenville Avenue<br><span>California, TX 70240</span></p>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Main Navigation-->
      <div class="main_menu menu_fixed nav-home-three">
        <div class="container">
          <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
              <div class="logo"><img src="{{asset('images/logo.jpg')}}" alt=""></div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="nav-menu pull-left text-left">
                <div class="nav-t-holder pull-left text-left">
                  <div class="nav-t-header">
                    <button><i class="fa fa-bars"></i></button>
                  </div>
                  <div class="nav-t-footer">
                    <ul class="nav">
                      <li class="">
                          <a href="{{URL::to('/')}}">Home</a>
                      </li>
                      <li class="has-t-submenu"><a href="about.html">about us</a>
                        <ul class="submenu">
                          <li><a href="team.html">Our Team</a></li>
                          <li><a href="testimonials.html">Testimonials</a></li>
                          <li><a href="faq.html">Faq</a></li>
                        </ul>
                      </li>
                      <li class="has-t-submenu"><a href="solutions.html">solutions</a>
                        <ul class="submenu">
                          <li><a href="meterial.html">meterial engineering</a></li>
                          <li><a href="agricultural.html">agricultural processing</a></li>
                          <li><a href="mechanical.html">Mechanical engineering</a></li>
                          <li><a href="chemical.html">chemical research</a></li>
                          <li><a href="power.html">power and energy</a></li>
                          <li><a href="oil.html">Oil and lubricant</a></li>
                          <li><a href="alternate.html">alternate energy</a></li>
                        </ul>
                      </li>
                      <li class="has-t-submenu"><a href="projects.html">Projects</a>
                        <ul class="submenu">
                          <li><a href="classic.html">classic</a></li>
                          <li><a href="fullscreen.html">fullscreen</a></li>
                          <li><a href="lightbox.html">lightbox</a></li>
                          <li><a href="projects-details.html">Projects Details</a></li>
                        </ul>
                      </li>
                      <li class="has-t-submenu"><a href="news.html">News</a>
                        <ul class="submenu">
                          <li><a href="news-details.html">News Details</a></li>
                        </ul>
                      </li>
                      <li class="has-t-submenu"><a href="shop.html">Shop</a>
                        <ul class="submenu">
                          <li><a href="product.html">Product Details</a></li>
                          <li><a href="cart.html">Cart Page</a></li>
                          <li><a href="checkout.html">Checkout Page</a></li>
                        </ul>
                      </li>
                      <li><a href="contact.html">contact us</a></li>
                    </ul>
                    <div class="mobile-link"><a href="request-qoute.html" class="submit">requet a quote</a>
                      <div class="widget-t widget-t-search">
                        <div class="widget-t-inner">
                          <form action="#" method="get" class="search-form">
                            <div class="input-group">
                              <input type="search" placeholder="Search" class="form-control"><span class="input-group-addon">
                                <button type="submit"><i class="icon icon-Search"></i></button></span>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="nav-search pull-right text-right">
                <div class="widget-t widget-t-search">
                  <div class="widget-t-inner">
                    <form action="#" method="get" class="search-form">
                      <div class="input-group">
                        <input type="search" placeholder="Search" class="form-control"><span class="input-group-addon">
                          <button type="submit"><i class="icon icon-Search"></i></button></span>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Main Navigation-->
    </header>
    <!-- =========home banner start============-->
    <div id="minimal-bootstrap-carousel" data-ride="carousel" class="carousel slide carousel-fade shop-slider full_width ver_new_1_slider">
      <!-- Wrapper for slides-->
      <div role="listbox" class="carousel-inner">
        <div style="background-image: url({{asset('images/slides/slider3-modified.png')}})" class="item active">
          <div class="carousel-caption">
            <div class="thm-container">
              <div class="box valign-top">
                <div class="content text-left pull-right">
                  <h1 data-animation="animated fadeInLeft" class="bnrfnt40">Leader in Solar Technologies and<br> Products</h1>
                  <p data-animation="animated fadeInRight" class="pln_he">OIC Solar is  a reliable Renewable Energy company <br>We provide a superior level of excellence<br> in the solar sector</p>
                  <a data-animation="animated fadeInUp" href="about_us.html" class="view-all hvr-bounce-to-right slide_learn_btn btn btn0">learn more</a><a data-animation="animated fadeInUp" href="about_us.html" class="view-all hvr-bounce-to-right slide_learn_btn btn">our sevices</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div style="background-image: url({{asset('images/slides/slider8-modified.png')}});" class="item">
          <div class="carousel-caption">
            <div class="thm-container">
              <div class="box valign-top">
                <div class="content text-left pull-right">
                  <h1 data-animation="animated fadeInLeft" class="bnrfnt40">We are Leaders in Field<br>for your daily need</h1>
                  <p data-animation="animated fadeInRight" class="pln_he">We provide solar systems for both commercial and home consumption<br> i.e houses, estates, companies, ministries <br> and many more</p><a data-animation="animated fadeInUp" href="about_us.html" class="view-all hvr-bounce-to-right slide_learn_btn btn btn0">learn more</a><a data-animation="animated fadeInUp" href="about_us.html" class="view-all hvr-bounce-to-right slide_learn_btn btn">our sevices</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div style="background-image: url({{asset('images/slides/slider4-modified.png')}});" class="item">
          <div class="carousel-caption">
            <div class="thm-container">
              <div class="box valign-top">
                <div class="content text-left pull-right">
                  <h1 data-animation="animated fadeInUp" class="bnrfnt40">We are very innovative<br> with consortium of experienced engineers</h1>
                  <p data-animation="animated fadeInDown" class="pln_he"> Our engineers are hardworking well-behaved and intelligent</p><a data-animation="animated fadeInUp" href="about_us.html" class="view-all hvr-bounce-to-right slide_learn_btn btn btn0">learn more</a><a data-animation="animated fadeInUp" href="about_us.html" class="view-all hvr-bounce-to-right slide_learn_btn btn">our sevices</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div style="background-image: url({{asset('images/slides/slide-product.png')}});" class="item">
            <div class="carousel-caption">
              <div class="thm-container">
                <div class="box valign-top">
                  <div class="content text-left pull-right">
                    <h1 data-animation="animated fadeInUp" class="bnrfnt40">We sell <br> solar-powered electronics</h1>
                    <p data-animation="animated fadeInDown" class="pln_he">Order your solar-powered electronics, devices <br>delivered to your doorstep without any hassle<br> 24/7 Support system</p><a data-animation="animated fadeInUp" href="about_us.html" class="view-all hvr-bounce-to-right slide_learn_btn btn btn0">learn more</a><a data-animation="animated fadeInUp" href="about_us.html" class="view-all hvr-bounce-to-right slide_learn_btn btn">our sevices</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
      <!-- Controls--><a href="#minimal-bootstrap-carousel" role="button" data-slide="prev" class="left carousel-control"><i class="fa fa-angle-left"></i><span class="sr-only">Previous</span></a><a href="#minimal-bootstrap-carousel" role="button" data-slide="next" class="right carousel-control"><i class="fa fa-angle-right"></i><span class="sr-only">Next</span></a>
    </div>
    <!-- =========home banner end============-->
    <!-- We offer Different Services-->
    <section class="diff-offer-wrapper">
      <div class="container">
        <div class="row diff-offer">
          <ul>
            <li class="we-offer-cont">
              <h2>We offer<span>Different Services</span></h2>
            </li>
            <li class="we-offer-cont2">
              <p>We have facilities and equipments to produce 21st century solar systems based on specially developed technology and designs. We are also ready to develop according to our customers' changing needs.</p>
            </li>
          </ul>
        </div>
        <div class="row">
          <div class="col-sm-4 service-info">
            <div class="item">
                <a href="agricultural.html" class="post-image view image_hover">
                <img src="{{asset('images/services/service-1.jpg')}}" alt="" class="img-responsive zoom_img_effect">
                </a>
            <a href="agricultural.html">
                <h4>Home, Offices & Schools</h4></a>
              <p>We design and install solar systems for homes, offices and schools at affordable prices</p>
              <h6><a href="mechanical.html">Read more</a></h6>
            </div>
          </div>
          <div class="col-sm-4 service-info">
            <div class="item">
                <a href="agricultural.html" class="post-image view image_hover">
                    <img src="{{asset('images/services/service-3.jpg')}}" alt="" class="img-responsive zoom_img_effect">
                </a>
                <a href="agricultural.html">
                <h4>Solar Farming</h4></a>
              <p>Solar farming is made possible with the use of solar-powered farming devices coupled with solar systems</p>
              <h6><a href="agricultural.html">Read more</a></h6>
            </div>
          </div>
          <div class="col-sm-4 service-info">
            <div class="item"><a href="oil.html" class="post-image view image_hover">	
                <img src="{{asset('images/services/service-2.jpg')}}" alt="" class="img-responsive zoom_img_effect"></a><a href="oil.html">
                <h4>Telecoms Masts</h4></a>
              <p>We provide alternative source of energy to power telecommunication masts</p>
              <h6><a href="oil.html">Read more</a></h6>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4 service-info">
            <div class="item">
                <a href="power.html" class="post-image view image_hover">
                    <img src="{{asset('images/services/service-6.jpg')}}" alt="" class="img-responsive zoom_img_effect">
                </a><a href="power.html">
                <h4>Capacity Buildings</h4></a>
              <p>We build solar system for capacity buildings such as stadium, lecture theatre, event centres, halls etc</p>
              <h6><a href="power.html">Read more</a></h6>
            </div>
          </div>
          <div class="col-sm-4 service-info">
            <div class="item">
                <a href="chemical.html" class="post-image view image_hover">
                    <img src="{{asset('images/services/service-8.jpg')}}" alt="" class="img-responsive zoom_img_effect"></a><a href="chemical.html">
                <h4>Solar ATMs</h4></a>
              <p>Automated teller machines powered by solar systems which runs 24/7</p>
              <h6><a href="chemical.html">Read more</a></h6>
            </div>
          </div>
          <div class="col-sm-4 service-info">
            <div class="item"><a href="meterial.html" class="post-image view image_hover">
                <img src="{{asset('images/services/service-7.jpg')}}" alt="" class="img-responsive zoom_img_effect"></a><a href="meterial.html">
                <h4>Street Lighting</h4></a>
              <p>Conventional street light powered by solar energy provided to meet the societal needs</p>
              <h6><a href="meterial.html">Read more</a></h6>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- We offer Different Services-->
    <!-- Control in compliance-->
    <section class="container ind-common-pad2 clearfix">
      <div class="quality-wrapper text-center">
        <div class="vision tab-panel fade in">
          <h2>A high level Quality Control in compliance with National and International regulations and standards</h2>
          <p>
                Our vision is to generate 1,000 MWH of power per day by 2025, from renewable sources
          </p>
        </div>
        <div class="values tab-panel fade hide">
                <h2>A high level Quality Control in compliance with National and International regulations and standards</h2>
                <p>
                Our values include world-class quality Products, attractive return on investment, customer satisfaction, dedicated maintenance team, very affordable energy solution 
          </p>
        </div>
        <div class="mission tab-panel fade hide">
                <h2>A high level Quality Control in compliance with National and International regulations and standards</h2>
                <p>
            To be a world-class green energy provider	
          </p>
        </div>
        <div class="vision-wrapper text-center">
          <ul>
            <li><a id="vision" href="javascript:void(0)"><img src="{{asset('images/services/ser-icon4.png')}}" alt="">
                <p>Vision</p></a></li>
            <li><a id="values" href="javascript:void(0)"><img src="{{asset('images/services/ser-icon5.png')}}" alt="">
                <p>Values</p></a></li>
            <li><a id="mission" href="javascript:void(0)"><img src="{{asset('images/services/ser-icon6.png')}}" alt="">
                <p>Mission</p></a></li>
          </ul>
        </div>
      </div>
    </section>
    <!-- Control in compliance-->
    <!-- Our Services natural resource-->
    <section class="fluid-service-area-home">
      <div class="work-image-ser"><img src="{{asset('images/services/ser-img-left.jpeg')}}" alt=""></div>
      <div class="service-promo">
        <div class="service-t-content">
          <div class="test-quote-sec"><img src="{{asset('images/quote-n.png')}}" alt=""></div>
          <div class="testimonial-t-sec">
            <div class="testimonialn-slider">
              <div class="item">
                <div class="name-content clearfix">
                  <div class="tst-img"><img src="{{asset('images/ceo.png')}}" alt=""></div>
                  <div class="client-name">
                    <p>OICSolar is a great firm<span>Ayanfe Olaoluwa (CEO & Founder)</span></p>
                    <ul>
                      <li><a href="#"><i class="fa fa-star"></i></a></li>
                      <li><a href="#"><i class="fa fa-star"></i></a></li>
                      <li><a href="#"><i class="fa fa-star"></i></a></li>
                      <li><a href="#"><i class="fa fa-star"></i></a></li>
                      <li><a href="#"><i class="fa fa-star"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="content clearfix">
                  <p>We are here to make the world a safer global community</p>
                </div>
                <div class="sign text-right"><img src="{{asset('images/sign-n.png')}}" alt=""></div>
              </div>
              <div class="item">
                <div class="name-content clearfix">
                  <div class="tst-img"><img src="{{asset('images/test-img2.png')}}" alt=""></div>
                  <div class="client-name">
                    <p>NanoTech <span> CEO</span></p>
                    <ul>
                      <li><a href="#"><i class="fa fa-star"></i></a></li>
                      <li><a href="#"><i class="fa fa-star"></i></a></li>
                      <li><a href="#"><i class="fa fa-star"></i></a></li>
                      <li><a href="#"><i class="fa fa-star"></i></a></li>
                      <li><a href="#"><i class="fa fa-star"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="content clearfix">
                  <p>We have a working relationship with OICSolar and i must say, their reputation precedes them</p>
                </div>
                <div class="sign text-right"><img src="{{asset('images/sign-n.png')}}" alt=""></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Our Services natural resource-->
    <!-- LOOKING AN ADEQUATE-->
    <section class="looking-wrapper clearfix">
      <div class="container">
        <div class="row">
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="indurial-solution-text2">
              <h2>LOOKING FOR AN ADEQUATE SOLAR SOLUTION FOR YOUR COMPANY?</h2>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-right">
            <div class="req-button text-right"><a href="contact.html" class="submit">Contact Us <i class="fa fa-arrow-right"></i></a></div>
          </div>
        </div>
      </div>
    </section>
    <!-- LOOKING AN ADEQUATE-->
    <!-- Latest News-->
    <section class="latest-news1 sectpad">
      <div class="container clearfix">
        <div class="row">
          <div class="col-lg-6 about-sec-content">
            <div class="section_header2 common">
              <h2>About Us</h2>
            </div>
            <h4>OIC Solar is  a reliable Renewable Energy company registered in Nigeria with RC number 814913 in 2009 and our vision is to generate 1,000 MWH of power per day by 2025, from renewable sources
                </h4>
            <p>We have technical expertise in design, installation and maintenance of Power Systems</p>
            <div class="row">
              <div class="col-md-6">
                <ul>
                  <li><i class="fa fa-arrow-circle-right"></i>Annual Company Growth of 40%</li>
                  <li><i class="fa fa-arrow-circle-right"></i>Over 50 Employed enginerrs</li>
                </ul>
              </div>
              <div class="col-md-6">
                <ul>
                  <li><i class="fa fa-arrow-circle-right"></i> Dedicated Maintenance team </li>
                  <li><i class="fa fa-arrow-circle-right"></i>Excellent Customer Relationship</li>
                </ul>
              </div>
            </div>
            <div class="req-button about-but text-left"><a href="about.html" class="submit">About Us <i class="fa fa-arrow-right"></i></a></div>
          </div>
          <div class="col-lg-6">
            <div class="section-faq">
              <div class="section_header2 common">
                <h2>Frequently Ask questions</h2>
              </div>
              <div class="accordian-area accordian-area-pad">
                <div id="accordion" role="tablist" aria-multiselectable="true" class="panel-group">
                  <div class="panel panel-default">
                    <div id="headingOne" role="tab" class="panel-heading">
                      <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                          <span>
                          How do i submit a request for OICSolar services?
                        </span><i class="fa fa-minus"></i><i class="fa fa-plus"></i></a></h4>
                    </div>
                    <div id="collapseOne" role="tabpanel" aria-labelledby="headingOne" class="panel-collapse collapse">
                      <div class="panel-body faq-content">
                        <p>You can contact us via the phone number in the top header or fill the contact form below. Our Customer support will contact you via email or give you a call</p>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div id="headingTwo" role="tab" class="panel-heading">
                      <h4 class="panel-title on"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                          Do you sell solar-powered electronics?
                        <i class="fa fa-minus"></i><i class="fa fa-plus"></i></a></h4>
                    </div>
                    <div id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo" class="panel-collapse collapse in">
                      <div class="panel-body faq-content">
                        <p>Yes, we do. Navigate to our shop section and select items of your choice. Happy shopping</p>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div id="headingThree" role="tab" class="panel-heading">
                      <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="collapsed">How do i submit a feedback, comment or make enquiries?<i class="fa fa-minus"></i><i class="fa fa-plus"></i></a></h4>
                    </div>
                    <div id="collapseThree" role="tabpanel" aria-labelledby="headingThree" class="panel-collapse collapse">
                      <div class="panel-body faq-content">
                        <p>
                            You can submit your enquiries, feedback and contributions via our contact form or send an email to our official email address at oicsolar@gmail.com
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Our Projects-->
    <section class="our-galler-htwo clearfix sectpad">
      <div class="container clearfix">
        <div class="section_header3 section_header2 common">
          <h2>Our Projects and Products</h2>
        </div>
      </div>
      <div class="fullwidth-silder">
        <div class="fullwidth-slider">
          <div class="item">
            <div class="img-holder"><a href="{{asset('images/projects/project-1.jpeg')}}" class="tt-gallery-1 lightbox"><span class="tt-gallery-1-overlay"></span>
                <div class="project-post-image image_hover"><img src="images/projects/project-1.jpeg" alt="Capacity building" class="zoom_img_effect"><span class="tt-gallery-1-caption"><span class="tt-gallery-1-caption-table"><span class="tt-gallery-1-caption-inner"><span class="tt-gallery-1-search"><i class="fa fa-search"></i></span></span></span></span></div></a></div>
          </div>
          <div class="item">
            <div class="img-holder"><a href="{{asset('images/projects/all-in-one-solar-street-light-30w.jpeg')}}" class="tt-gallery-1 lightbox"><span class="tt-gallery-1-overlay"></span>
                <div class="project-post-image image_hover"><img src="images/projects/all-in-one-solar-street-light-30w.jpeg" alt="All in one solar street light 30w" class="zoom_img_effect"><span class="tt-gallery-1-caption"><span class="tt-gallery-1-caption-table"><span class="tt-gallery-1-caption-inner"><span class="tt-gallery-1-search"><i class="fa fa-search"></i></span></span></span></span></div></a></div>
          </div>
          <div class="item">
            <div class="img-holder"><a href="{{asset('images/projects/solar-powered-flood-light.jpeg')}}" class="tt-gallery-1 lightbox"><span class="tt-gallery-1-overlay"></span>
                <div class="project-post-image image_hover"><img src="images/projects/solar-powered-flood-light.jpeg" alt="Solar powered flood light" class="zoom_img_effect"><span class="tt-gallery-1-caption"><span class="tt-gallery-1-caption-table"><span class="tt-gallery-1-caption-inner"><span class="tt-gallery-1-search"><i class="fa fa-search"></i></span></span></span></span></div></a></div>
          </div>
          <div class="item">
            <div class="img-holder"><a href="{{asset('images/projects/solar-led-lights.png')}}" class="tt-gallery-1 lightbox"><span class="tt-gallery-1-overlay"></span>
                <div class="project-post-image image_hover"><img src="images/projects/solar-led-lights.png" alt="Solar led lights" class="zoom_img_effect"><span class="tt-gallery-1-caption"><span class="tt-gallery-1-caption-table"><span class="tt-gallery-1-caption-inner"><span class="tt-gallery-1-search"><i class="fa fa-search"></i></span></span></span></span></div></a></div>
          </div>
          <div class="item">
            <div class="img-holder"><a href="{{asset('images/projects/solar-powered-water-pump.jpeg')}}" class="tt-gallery-1 lightbox"><span class="tt-gallery-1-overlay"></span>
                <div class="project-post-image image_hover"><img src="images/projects/solar-powered-water-pump.jpeg" alt="Solar powered water pump" class="zoom_img_effect"><span class="tt-gallery-1-caption"><span class="tt-gallery-1-caption-table"><span class="tt-gallery-1-caption-inner"><span class="tt-gallery-1-search"><i class="fa fa-search"></i></span></span></span></span></div></a></div>
          </div>
        </div>
      </div>
    </section>
    <!-- Lattest news-->
    <div class="container clearfix sectpad-sec">
      <div class="section_header2 common">
        <h2>More products</h2>
      </div>
      <div class="row event-pad">
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="news-evn-img"><a href="news.html" class="image_hover"><img src="images/products/solar-blender.png" alt="" class="img-responsive zoom_img_effect"></a>
            <div class="event-date">
              <h3>26 <small>Dec</small></h3>
            </div>
          </div>
          <div class="news-evn-cont">
            <div class="news-meta"><a href="news.html">Price: NGN 15000</a></div><a href="news.html">
              <h3>Solar blender</h3></a>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="news-evn-img"><a href="news.html" class="image_hover">
              <img src="images/products/solar-street-light.png" alt="" class="img-responsive zoom_img_effect"></a>
            <div class="event-date">
              <h3>26 <small>Dec</small></h3>
            </div>
          </div>
          <div class="news-evn-cont">
            <div class="news-meta"><a href="news.html">Price: NGN 16000</a><a href="news.html">20W Dc ﬂood light</a></div><a href="news.html">
              <h3>Solar street light</h3></a>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="news-evn-img"><a href="news.html" class="image_hover"><img src="images/products/solar-powered-rice-cooker.png" alt="" class="img-responsive zoom_img_effect"></a>
            <div class="event-date">
              <h3>26 <small>Dec</small></h3>
            </div>
          </div>
          <div class="news-evn-cont">
            <div class="news-meta"><a href="news.html">Price: NGN 25000</a>(30 Litres)</div><a href="news.html">
              <h3>Solar powered rice cooker</h3></a>
          </div>
        </div>
      </div>
    </div>
    <!-- product solutions-->
    <section class="our-sol-wrapper clearfix">
      <div class="container clearfix">
        <p>We provide innovative <span>product solutions</span> for sustainable progress. Our professional team works to increase productivity and cost effectiveness on the market.</p>
      </div>
    </section>
    <!-- our clients & Get in touch-->
    <div class="container clearfix ind-common-pad">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 our-t-client">
          <div class="section_header2 common">
            <h2>our partners</h2>
          </div>
          <p> We are partner to a global leader in the solar retail business and together we are pioneering solar power revolution in Africa</p>
          <ul>
            <li><img src="images/partners/simba.png" alt="" class="img-responsive"></li>
            <li><img src="images/partners/lautech.png" alt="" class="img-responsive"></li>
            <li><img src="images/partners/funaab.png" alt="" class="img-responsive"></li>
          </ul>
          <ul>
            <li><img src="images/partners/10.jpg" alt="" class="img-responsive"></li>
            <li><img src="images/partners/11.jpg" alt="" class="img-responsive"></li>
            <li><img src="images/partners/12.jpg" alt="" class="img-responsive"></li>
          </ul>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="section_header2 common">
            <h2>Get in touch</h2>
          </div>
          <div class="get-t-touch">
            <div class="get-t-touch-inner input_form service-request-form">
              <form id="serviceForm" action="service_request.php" class="service_request">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <input id="name" type="text" name="name" placeholder="Your Name:" class="form-control">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <input id="email" type="email" name="email" placeholder="Your Email:" class="form-control">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="select-box">
                    <select id="selectService" name="selectService" class="select-menu">
                      <option value="0" selected="">Select One</option>
                      <option value="Mechanical Engineering">Mechanical Engineering</option>
                      <option value="Agricultural Processing">Agricultural Processing</option>
                      <option value="Power and Engery">Power and Engery</option>
                      <option value="Chemical Research">Chemical Research</option>
                      <option value="Meterial engineering">Meterial engineering</option>
                    </select>
                  </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <input id="phone" type="text" name="phone" placeholder="Your Phone Number:" class="form-control">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 submit-n-now">
                  <button type="submit" class="submit">Submit Now <i class="fa fa-arrow-right"></i></button>
                </div>
              </form>
              <div id="success">
                <p>Your request sent successfully.</p>
              </div>
              <div id="error">
                <p>Something is wrong. Message cant be sent!</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Indurial Solution-->
    <section class="indurial-t-solution indurial-solution indpad anim-5-all indurial-t-solution3">
      <div class="container clearfix">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="indurial-solution-text text-center">
              <h2>If  You Need Any Solar Solution ... We Are Available For You</h2><span class="contactus-button2 text-center"><a href="contact.html" class="submit">Contact Us </a></span>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Indurial Solution-->
    <footer class="sec-padding footer-bg footer-bg3">
      <div class="container clearfix">
        <div class="row">
          <div class="widget about-us-widget col-md-3 col-sm-6"><a href="#"><img src="images/header/f-logo2.png" alt=""></a>
            <p>We have a proven track record of delivering  exceptional results with attractive ROI on every project executed</p><a href="about.html">Read More <i class="fa fa-angle-double-right"></i></a>
            <ul class="nav">
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
              <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
              <li><a href="#"><i class="fa fa-skype"></i></a></li>
              <li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
            </ul>
          </div>
          <div class="widget widget-links col-md-3 col-sm-6">
            <h4 class="widget_title">Our Solutions</h4>
            <div class="widget-contact-list row m0">
              <ul>
                <li><a href="meterial.html">- Home, offices and schools</a></li>
                <li><a href="agricultural.html">- Solar farming</a></li>
                <li><a href="mechanical.html">- Telecoms Mast</a></li>
                <li><a href="chemical.html">- Capacity buildings</a></li>
                <li><a href="power.html">- Solar ATMs</a></li>
                <li><a href="oil.html">- Street Lighting</a></li>
              </ul>
            </div>
          </div>
          <div class="widget widget-links col-md-3 col-sm-6">
            <h4 class="widget_title">Quick Links</h4>
            <div class="widget-contact-list row m0">
              <ul>
                <li><a href="about.html">- About Us</a></li>
                <li><a href="#">- Career</a></li>
                <li><a href="#">- Get Quote</a></li>
                <li><a href="">- Shop</a></li>
                <li><a href="faq.html">- Faq</a></li>
              </ul>
            </div>
          </div>
          <div class="widget widget-contact col-md-3 col-sm-6">
            <h4 class="widget_title">Get in Touch</h4>
            <div class="widget-contact-list row m0">
              <ul>
                <li><i class="fa fa-map-marker"></i>
                  <div class="fleft location_address">
                        TmeLine Plaza FUNAAB Road, Abeokuta
                        35, Abiola-Way Beside Albarka Mosque
                        </div>
                </li>
                <li><i class="fa fa-phone"></i>
                  <div class="fleft contact_no"><a href="#">2348105020388</a></div>
                </li>
                <li><i class="fa fa-envelope-o"></i>
                  <div class="fleft contact_mail"><a href="mailto:oicsolar@gmail.com">oicsolar@gmail.com</a></div>
                </li>
                <li><i class="icon icon-Timer"></i>
                  <div class="fleft service_time">Mon - Sat : 8am to 4pm</div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <section class="footer-t-bottom footer-bottom footer-bottom3">
      <div class="container clearfix">
        <div class="pull-left fo-txt">
          <p>Copyright © OICSolar 2017.. All rights reserved.</p>
        </div>
        <div class="pull-right fo-txt">
          <p>Created by: <a href="http://nano.ly1p.com.ng/">NanoTech</a></p>
        </div>
      </div>
    </section>
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- Revolution Slider Tools-->
    <script src="{{asset('js/jquery.themepunch.revolution.min.js')}}"></script>
    <!-- Revolution Slider-->
    <script type="text/javascript" src="{{asset('js/revolution.extension.slideanims.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/revolution.extension.layeranimation.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/revolution.extension.navigation.min.js')}}"></script>
    <script src="{{asset('js/jquery.form.js')}}"></script>
    <script src="{{asset('js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('js/contact.js')}}"></script>
    <!-- owl carousel-->
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTI1BZJKFCeJ8g9O01CMZ4QvVCfdPYkrE"></script>
    <script src="{{asset('js/gmaps.min.js')}}"></script>
    <script src="{{asset('js/imagelightbox.min.js')}}"></script>
    <script src="{{asset('js/theme.js')}}"></script>
  </body>
</html>


<!--Drop roll back full laravel dummy -->


 <!-- =========home banner start============-->
 <div id="minimal-bootstrap-carousel" data-ride="carousel" class="carousel slide carousel-fade shop-slider full_width ver_new_1_slider">
    <!-- Wrapper for slides-->
    <div role="listbox" class="carousel-inner">
      <div style="background-image: url({{asset('images/slides/slider3-modified.png')}})" class="item active">
        <div class="carousel-caption">
          <div class="thm-container">
            <div class="box valign-top">
              <div class="content text-left pull-right">
                <h1 data-animation="animated fadeInLeft" class="bnrfnt40">Leader in Solar Technologies and<br> Products</h1>
                <p data-animation="animated fadeInRight" class="pln_he">OIC Solar is  a reliable Renewable Energy company <br>We provide a superior level of excellence<br> in the solar sector</p>
                <a data-animation="animated fadeInUp" href="about_us.html" class="view-all hvr-bounce-to-right slide_learn_btn btn btn0">learn more</a><a data-animation="animated fadeInUp" href="about_us.html" class="view-all hvr-bounce-to-right slide_learn_btn btn">our sevices</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div style="background-image: url({{asset('images/slides/slider8-modified.png')}});" class="item">
        <div class="carousel-caption">
          <div class="thm-container">
            <div class="box valign-top">
              <div class="content text-left pull-right">
                <h1 data-animation="animated fadeInLeft" class="bnrfnt40">We are Leaders in Field<br>for your daily need</h1>
                <p data-animation="animated fadeInRight" class="pln_he">We provide solar systems for both commercial and home consumption<br> i.e houses, estates, companies, ministries <br> and many more</p><a data-animation="animated fadeInUp" href="about_us.html" class="view-all hvr-bounce-to-right slide_learn_btn btn btn0">learn more</a><a data-animation="animated fadeInUp" href="about_us.html" class="view-all hvr-bounce-to-right slide_learn_btn btn">our sevices</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div style="background-image: url({{asset('images/slides/slider4-modified.png')}});" class="item">
        <div class="carousel-caption">
          <div class="thm-container">
            <div class="box valign-top">
              <div class="content text-left pull-right">
                <h1 data-animation="animated fadeInUp" class="bnrfnt40">We are very innovative<br> with consortium of experienced engineers</h1>
                <p data-animation="animated fadeInDown" class="pln_he"> Our engineers are hardworking well-behaved and intelligent</p><a data-animation="animated fadeInUp" href="about_us.html" class="view-all hvr-bounce-to-right slide_learn_btn btn btn0">learn more</a><a data-animation="animated fadeInUp" href="about_us.html" class="view-all hvr-bounce-to-right slide_learn_btn btn">our sevices</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div style="background-image: url({{asset('images/slides/slide-product.png')}});" class="item">
          <div class="carousel-caption">
            <div class="thm-container">
              <div class="box valign-top">
                <div class="content text-left pull-right">
                  <h1 data-animation="animated fadeInUp" class="bnrfnt40">We sell <br> solar-powered electronics</h1>
                  <p data-animation="animated fadeInDown" class="pln_he">Order your solar-powered electronics, devices <br>delivered to your doorstep without any hassle<br> 24/7 Support system</p><a data-animation="animated fadeInUp" href="about_us.html" class="view-all hvr-bounce-to-right slide_learn_btn btn btn0">learn more</a><a data-animation="animated fadeInUp" href="about_us.html" class="view-all hvr-bounce-to-right slide_learn_btn btn">our sevices</a>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    <!-- Controls--><a href="#minimal-bootstrap-carousel" role="button" data-slide="prev" class="left carousel-control"><i class="fa fa-angle-left"></i><span class="sr-only">Previous</span></a><a href="#minimal-bootstrap-carousel" role="button" data-slide="next" class="right carousel-control"><i class="fa fa-angle-right"></i><span class="sr-only">Next</span></a>
  </div>
  <!-- =========home banner end============-->
  <!-- We offer Different Services-->
  <section class="diff-offer-wrapper">
    <div class="container">
      <div class="row diff-offer">
        <ul>
          <li class="we-offer-cont">
            <h2>We offer<span>Different Services</span></h2>
          </li>
          <li class="we-offer-cont2">
            <p>We have facilities and equipments to produce 21st century solar systems based on specially developed technology and designs. We are also ready to develop according to our customers' changing needs.</p>
          </li>
        </ul>
      </div>
      <div class="row">
        <div class="col-sm-4 service-info">
          <div class="item">
              <a href="agricultural.html" class="post-image view image_hover">
              <img src="{{asset('images/services/service-1.jpg')}}" alt="" class="img-responsive zoom_img_effect">
              </a>
          <a href="agricultural.html">
              <h4>Home, Offices & Schools</h4></a>
            <p>We design and install solar systems for homes, offices and schools at affordable prices</p>
            <h6><a href="mechanical.html">Read more</a></h6>
          </div>
        </div>
        <div class="col-sm-4 service-info">
          <div class="item">
              <a href="agricultural.html" class="post-image view image_hover">
                  <img src="{{asset('images/services/service-3.jpg')}}" alt="" class="img-responsive zoom_img_effect">
              </a>
              <a href="agricultural.html">
              <h4>Solar Farming</h4></a>
            <p>Solar farming is made possible with the use of solar-powered farming devices coupled with solar systems</p>
            <h6><a href="agricultural.html">Read more</a></h6>
          </div>
        </div>
        <div class="col-sm-4 service-info">
          <div class="item"><a href="oil.html" class="post-image view image_hover">	
              <img src="{{asset('images/services/service-2.jpg')}}" alt="" class="img-responsive zoom_img_effect"></a><a href="oil.html">
              <h4>Telecoms Masts</h4></a>
            <p>We provide alternative source of energy to power telecommunication masts</p>
            <h6><a href="oil.html">Read more</a></h6>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4 service-info">
          <div class="item">
              <a href="power.html" class="post-image view image_hover">
                  <img src="{{asset('images/services/service-6.jpg')}}" alt="" class="img-responsive zoom_img_effect">
              </a><a href="power.html">
              <h4>Capacity Buildings</h4></a>
            <p>We build solar system for capacity buildings such as stadium, lecture theatre, event centres, halls etc</p>
            <h6><a href="power.html">Read more</a></h6>
          </div>
        </div>
        <div class="col-sm-4 service-info">
          <div class="item">
              <a href="chemical.html" class="post-image view image_hover">
                  <img src="{{asset('images/services/service-8.jpg')}}" alt="" class="img-responsive zoom_img_effect"></a><a href="chemical.html">
              <h4>Solar ATMs</h4></a>
            <p>Automated teller machines powered by solar systems which runs 24/7</p>
            <h6><a href="chemical.html">Read more</a></h6>
          </div>
        </div>
        <div class="col-sm-4 service-info">
          <div class="item"><a href="meterial.html" class="post-image view image_hover">
              <img src="{{asset('images/services/service-7.jpg')}}" alt="" class="img-responsive zoom_img_effect"></a><a href="meterial.html">
              <h4>Street Lighting</h4></a>
            <p>Conventional street light powered by solar energy provided to meet the societal needs</p>
            <h6><a href="meterial.html">Read more</a></h6>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- We offer Different Services-->
  <!-- Control in compliance-->
  <section class="container ind-common-pad2 clearfix">
    <div class="quality-wrapper text-center">
      <div class="vision tab-panel fade in">
        <h2>A high level Quality Control in compliance with National and International regulations and standards</h2>
        <p>
              Our vision is to generate 1,000 MWH of power per day by 2025, from renewable sources
        </p>
      </div>
      <div class="values tab-panel fade hide">
              <h2>A high level Quality Control in compliance with National and International regulations and standards</h2>
              <p>
              Our values include world-class quality Products, attractive return on investment, customer satisfaction, dedicated maintenance team, very affordable energy solution 
        </p>
      </div>
      <div class="mission tab-panel fade hide">
              <h2>A high level Quality Control in compliance with National and International regulations and standards</h2>
              <p>
          To be a world-class green energy provider	
        </p>
      </div>
      <div class="vision-wrapper text-center">
        <ul>
          <li><a id="vision" href="javascript:void(0)"><img src="{{asset('images/services/ser-icon4.png')}}" alt="">
              <p>Vision</p></a></li>
          <li><a id="values" href="javascript:void(0)"><img src="{{asset('images/services/ser-icon5.png')}}" alt="">
              <p>Values</p></a></li>
          <li><a id="mission" href="javascript:void(0)"><img src="{{asset('images/services/ser-icon6.png')}}" alt="">
              <p>Mission</p></a></li>
        </ul>
      </div>
    </div>
  </section>
  <!-- Control in compliance-->
  <!-- Our Services natural resource-->
  <section class="fluid-service-area-home">
    <div class="work-image-ser"><img src="{{asset('images/services/ser-img-left.jpeg')}}" alt=""></div>
    <div class="service-promo">
      <div class="service-t-content">
        <div class="test-quote-sec"><img src="{{asset('images/quote-n.png')}}" alt=""></div>
        <div class="testimonial-t-sec">
          <div class="testimonialn-slider">
            <div class="item">
              <div class="name-content clearfix">
                <div class="tst-img"><img src="{{asset('images/ceo.png')}}" alt=""></div>
                <div class="client-name">
                  <p>OICSolar is a great firm<span>Ayanfe Olaoluwa (CEO & Founder)</span></p>
                  <ul>
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="content clearfix">
                <p>We are here to make the world a safer global community</p>
              </div>
              <div class="sign text-right"><img src="{{asset('images/sign-n.png')}}" alt=""></div>
            </div>
            <div class="item">
              <div class="name-content clearfix">
                <div class="tst-img"><img src="{{asset('images/test-img2.png')}}" alt=""></div>
                <div class="client-name">
                  <p>NanoTech <span> CEO</span></p>
                  <ul>
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="content clearfix">
                <p>We have a working relationship with OICSolar and i must say, their reputation precedes them</p>
              </div>
              <div class="sign text-right"><img src="{{asset('images/sign-n.png')}}" alt=""></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Our Services natural resource-->
  <!-- LOOKING AN ADEQUATE-->
  <section class="looking-wrapper clearfix">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          <div class="indurial-solution-text2">
            <h2>LOOKING FOR AN ADEQUATE SOLAR SOLUTION FOR YOUR COMPANY?</h2>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-right">
          <div class="req-button text-right"><a href="contact.html" class="submit">Contact Us <i class="fa fa-arrow-right"></i></a></div>
        </div>
      </div>
    </div>
  </section>
  <!-- LOOKING AN ADEQUATE-->
  <!-- Latest News-->
  <section class="latest-news1 sectpad">
    <div class="container clearfix">
      <div class="row">
        <div class="col-lg-6 about-sec-content">
          <div class="section_header2 common">
            <h2>About Us</h2>
          </div>
          <h4>OIC Solar is  a reliable Renewable Energy company registered in Nigeria with RC number 814913 in 2009 and our vision is to generate 1,000 MWH of power per day by 2025, from renewable sources
              </h4>
          <p>We have technical expertise in design, installation and maintenance of Power Systems</p>
          <div class="row">
            <div class="col-md-6">
              <ul>
                <li><i class="fa fa-arrow-circle-right"></i>Annual Company Growth of 40%</li>
                <li><i class="fa fa-arrow-circle-right"></i>Over 50 Employed enginerrs</li>
              </ul>
            </div>
            <div class="col-md-6">
              <ul>
                <li><i class="fa fa-arrow-circle-right"></i> Dedicated Maintenance team </li>
                <li><i class="fa fa-arrow-circle-right"></i>Excellent Customer Relationship</li>
              </ul>
            </div>
          </div>
          <div class="req-button about-but text-left"><a href="about.html" class="submit">About Us <i class="fa fa-arrow-right"></i></a></div>
        </div>
        <div class="col-lg-6">
          <div class="section-faq">
            <div class="section_header2 common">
              <h2>Frequently Ask questions</h2>
            </div>
            <div class="accordian-area accordian-area-pad">
              <div id="accordion" role="tablist" aria-multiselectable="true" class="panel-group">
                <div class="panel panel-default">
                  <div id="headingOne" role="tab" class="panel-heading">
                    <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                        <span>
                        How do i submit a request for OICSolar services?
                      </span><i class="fa fa-minus"></i><i class="fa fa-plus"></i></a></h4>
                  </div>
                  <div id="collapseOne" role="tabpanel" aria-labelledby="headingOne" class="panel-collapse collapse">
                    <div class="panel-body faq-content">
                      <p>You can contact us via the phone number in the top header or fill the contact form below. Our Customer support will contact you via email or give you a call</p>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div id="headingTwo" role="tab" class="panel-heading">
                    <h4 class="panel-title on"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                        Do you sell solar-powered electronics?
                      <i class="fa fa-minus"></i><i class="fa fa-plus"></i></a></h4>
                  </div>
                  <div id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo" class="panel-collapse collapse in">
                    <div class="panel-body faq-content">
                      <p>Yes, we do. Navigate to our shop section and select items of your choice. Happy shopping</p>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div id="headingThree" role="tab" class="panel-heading">
                    <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="collapsed">How do i submit a feedback, comment or make enquiries?<i class="fa fa-minus"></i><i class="fa fa-plus"></i></a></h4>
                  </div>
                  <div id="collapseThree" role="tabpanel" aria-labelledby="headingThree" class="panel-collapse collapse">
                    <div class="panel-body faq-content">
                      <p>
                          You can submit your enquiries, feedback and contributions via our contact form or send an email to our official email address at oicsolar@gmail.com
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Our Projects-->
  <section class="our-galler-htwo clearfix sectpad">
    <div class="container clearfix">
      <div class="section_header3 section_header2 common">
        <h2>Our Projects and Products</h2>
      </div>
    </div>
    <div class="fullwidth-silder">
      <div class="fullwidth-slider">
        <div class="item">
          <div class="img-holder"><a href="{{asset('images/projects/project-1.jpeg')}}" class="tt-gallery-1 lightbox"><span class="tt-gallery-1-overlay"></span>
              <div class="project-post-image image_hover"><img src="images/projects/project-1.jpeg" alt="Capacity building" class="zoom_img_effect"><span class="tt-gallery-1-caption"><span class="tt-gallery-1-caption-table"><span class="tt-gallery-1-caption-inner"><span class="tt-gallery-1-search"><i class="fa fa-search"></i></span></span></span></span></div></a></div>
        </div>
        <div class="item">
          <div class="img-holder"><a href="{{asset('images/projects/all-in-one-solar-street-light-30w.jpeg')}}" class="tt-gallery-1 lightbox"><span class="tt-gallery-1-overlay"></span>
              <div class="project-post-image image_hover"><img src="images/projects/all-in-one-solar-street-light-30w.jpeg" alt="All in one solar street light 30w" class="zoom_img_effect"><span class="tt-gallery-1-caption"><span class="tt-gallery-1-caption-table"><span class="tt-gallery-1-caption-inner"><span class="tt-gallery-1-search"><i class="fa fa-search"></i></span></span></span></span></div></a></div>
        </div>
        <div class="item">
          <div class="img-holder"><a href="{{asset('images/projects/solar-powered-flood-light.jpeg')}}" class="tt-gallery-1 lightbox"><span class="tt-gallery-1-overlay"></span>
              <div class="project-post-image image_hover"><img src="images/projects/solar-powered-flood-light.jpeg" alt="Solar powered flood light" class="zoom_img_effect"><span class="tt-gallery-1-caption"><span class="tt-gallery-1-caption-table"><span class="tt-gallery-1-caption-inner"><span class="tt-gallery-1-search"><i class="fa fa-search"></i></span></span></span></span></div></a></div>
        </div>
        <div class="item">
          <div class="img-holder"><a href="{{asset('images/projects/solar-led-lights.png')}}" class="tt-gallery-1 lightbox"><span class="tt-gallery-1-overlay"></span>
              <div class="project-post-image image_hover"><img src="images/projects/solar-led-lights.png" alt="Solar led lights" class="zoom_img_effect"><span class="tt-gallery-1-caption"><span class="tt-gallery-1-caption-table"><span class="tt-gallery-1-caption-inner"><span class="tt-gallery-1-search"><i class="fa fa-search"></i></span></span></span></span></div></a></div>
        </div>
        <div class="item">
          <div class="img-holder"><a href="{{asset('images/projects/solar-powered-water-pump.jpeg')}}" class="tt-gallery-1 lightbox"><span class="tt-gallery-1-overlay"></span>
              <div class="project-post-image image_hover"><img src="images/projects/solar-powered-water-pump.jpeg" alt="Solar powered water pump" class="zoom_img_effect"><span class="tt-gallery-1-caption"><span class="tt-gallery-1-caption-table"><span class="tt-gallery-1-caption-inner"><span class="tt-gallery-1-search"><i class="fa fa-search"></i></span></span></span></span></div></a></div>
        </div>
      </div>
    </div>
  </section>
  <!-- Lattest news-->
  <div class="container clearfix sectpad-sec">
    <div class="section_header2 common">
      <h2>More products</h2>
    </div>
    <div class="row event-pad">
      <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="news-evn-img"><a href="news.html" class="image_hover"><img src="images/products/solar-blender.png" alt="" class="img-responsive zoom_img_effect"></a>
          <div class="event-date">
            <h3>26 <small>Dec</small></h3>
          </div>
        </div>
        <div class="news-evn-cont">
          <div class="news-meta"><a href="news.html">Price: NGN 15000</a></div><a href="news.html">
            <h3>Solar blender</h3></a>
        </div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="news-evn-img"><a href="news.html" class="image_hover">
            <img src="images/products/solar-street-light.png" alt="" class="img-responsive zoom_img_effect"></a>
          <div class="event-date">
            <h3>26 <small>Dec</small></h3>
          </div>
        </div>
        <div class="news-evn-cont">
          <div class="news-meta"><a href="news.html">Price: NGN 16000</a><a href="news.html">20W Dc ﬂood light</a></div><a href="news.html">
            <h3>Solar street light</h3></a>
        </div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="news-evn-img"><a href="news.html" class="image_hover"><img src="images/products/solar-powered-rice-cooker.png" alt="" class="img-responsive zoom_img_effect"></a>
          <div class="event-date">
            <h3>26 <small>Dec</small></h3>
          </div>
        </div>
        <div class="news-evn-cont">
          <div class="news-meta"><a href="news.html">Price: NGN 25000</a>(30 Litres)</div><a href="news.html">
            <h3>Solar powered rice cooker</h3></a>
        </div>
      </div>
    </div>
  </div>
  <!-- product solutions-->
  <section class="our-sol-wrapper clearfix">
    <div class="container clearfix">
      <p>We provide innovative <span>product solutions</span> for sustainable progress. Our professional team works to increase productivity and cost effectiveness on the market.</p>
    </div>
  </section>
  <!-- our clients & Get in touch-->
  <div class="container clearfix ind-common-pad">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 our-t-client">
        <div class="section_header2 common">
          <h2>our partners</h2>
        </div>
        <p> We are partner to a global leader in the solar retail business and together we are pioneering solar power revolution in Africa</p>
        <ul>
          <li><img src="images/partners/simba.png" alt="" class="img-responsive"></li>
          <li><img src="images/partners/lautech.png" alt="" class="img-responsive"></li>
          <li><img src="images/partners/funaab.png" alt="" class="img-responsive"></li>
        </ul>
        <ul>
          <li><img src="images/partners/10.jpg" alt="" class="img-responsive"></li>
          <li><img src="images/partners/11.jpg" alt="" class="img-responsive"></li>
          <li><img src="images/partners/12.jpg" alt="" class="img-responsive"></li>
        </ul>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="section_header2 common">
          <h2>Get in touch</h2>
        </div>
        <div class="get-t-touch">
          <div class="get-t-touch-inner input_form service-request-form">
            <form id="serviceForm" action="service_request.php" class="service_request">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input id="name" type="text" name="name" placeholder="Your Name:" class="form-control">
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input id="email" type="email" name="email" placeholder="Your Email:" class="form-control">
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="select-box">
                  <select id="selectService" name="selectService" class="select-menu">
                    <option value="0" selected="">Select One</option>
                    <option value="Mechanical Engineering">Mechanical Engineering</option>
                    <option value="Agricultural Processing">Agricultural Processing</option>
                    <option value="Power and Engery">Power and Engery</option>
                    <option value="Chemical Research">Chemical Research</option>
                    <option value="Meterial engineering">Meterial engineering</option>
                  </select>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input id="phone" type="text" name="phone" placeholder="Your Phone Number:" class="form-control">
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 submit-n-now">
                <button type="submit" class="submit">Submit Now <i class="fa fa-arrow-right"></i></button>
              </div>
            </form>
            <div id="success">
              <p>Your request sent successfully.</p>
            </div>
            <div id="error">
              <p>Something is wrong. Message cant be sent!</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>