

      <!-- Indurial Solution-->
<section class="indurial-t-solution indurial-solution indpad anim-5-all indurial-t-solution3">
        <div class="container clearfix">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="indurial-solution-text text-center">
                <h2>If  You Need Any Solar Solution ... We Are Available For You</h2><span class="contactus-button2 text-center"><a href="contact.html" class="submit">Contact Us </a></span>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Indurial Solution-->
      <footer class="sec-padding footer-bg footer-bg3">
        <div class="container clearfix">
          <div class="row">
            <div class="widget about-us-widget col-md-3 col-sm-6"><a href="#"><img src="images/header/f-logo2.png" alt=""></a>
              <p>We have a proven track record of delivering  exceptional results with attractive ROI on every project executed</p><a href="about.html">Read More <i class="fa fa-angle-double-right"></i></a>
              <ul class="nav">
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                <li><a href="#"><i class="fa fa-skype"></i></a></li>
                <li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
              </ul>
            </div>
            <div class="widget widget-links col-md-3 col-sm-6">
              <h4 class="widget_title">Our Solutions</h4>
              <div class="widget-contact-list row m0">
                <ul>
                  <li><a href="meterial.html">- Home, offices and schools</a></li>
                  <li><a href="agricultural.html">- Solar farming</a></li>
                  <li><a href="mechanical.html">- Telecoms Mast</a></li>
                  <li><a href="chemical.html">- Capacity buildings</a></li>
                  <li><a href="power.html">- Solar ATMs</a></li>
                  <li><a href="oil.html">- Street Lighting</a></li>
                </ul>
              </div>
            </div>
            <div class="widget widget-links col-md-3 col-sm-6">
              <h4 class="widget_title">Quick Links</h4>
              <div class="widget-contact-list row m0">
                <ul>
                  <li><a href="about.html">- About Us</a></li>
                  <li><a href="#">- Career</a></li>
                  <li><a href="#">- Get Quote</a></li>
                  <li><a href="">- Shop</a></li>
                  <li><a href="faq.html">- Faq</a></li>
                </ul>
              </div>
            </div>
            <div class="widget widget-contact col-md-3 col-sm-6">
              <h4 class="widget_title">Get in Touch</h4>
              <div class="widget-contact-list row m0">
                <ul>
                  <li><i class="fa fa-map-marker"></i>
                    <div class="fleft location_address">
                          TmeLine Plaza FUNAAB Road, Abeokuta
                          35, Abiola-Way Beside Albarka Mosque
                          </div>
                  </li>
                  <li><i class="fa fa-phone"></i>
                    <div class="fleft contact_no"><a href="#">2348105020388</a></div>
                  </li>
                  <li><i class="fa fa-envelope-o"></i>
                    <div class="fleft contact_mail"><a href="mailto:oicsolar@gmail.com">oicsolar@gmail.com</a></div>
                  </li>
                  <li><i class="icon icon-Timer"></i>
                    <div class="fleft service_time">Mon - Sat : 8am to 4pm</div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <section class="footer-t-bottom footer-bottom footer-bottom3">
        <div class="container clearfix">
          <div class="pull-left fo-txt">
            <p>Copyright © OICSolar 2017.. All rights reserved.</p>
          </div>
          <div class="pull-right fo-txt">
            <p>Created by: <a href="http://nano.ly1p.com.ng/">NanoTech</a></p>
          </div>
        </div>
      </section>