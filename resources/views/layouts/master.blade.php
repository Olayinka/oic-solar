<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <meta name="keywords" content="OICSolar Solutions">
      <meta name="description" content="We provide your Solar solutions and products">
    <meta name="author" content="NanoTech Team">
      
    <!-- reponsive meta-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OICSolar Solutions | Homepage</title>
    <!-- Bootstrap-->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- animate css-->
    <link rel="stylesheet" href="{{asset('css/animate.min.css')}}">
    <!-- owl-carousel-->
    <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('css/settings.css')}}">
    <link rel="stylesheet" href="{{asset('css/layers.css')}}">
    <link rel="stylesheet" href="{{asset('css/navigation.css')}}">
    <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
    <!-- Main Css-->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries-->
    <!-- WARNING: Respond.js doesn't work if you view the page via file://-->
    <!--if lt IE 9
    script(src='https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js')
    script(src='https://oss.maxcdn.com/respond/1.4.2/respond.min.js')
    -->
  </head>
  <body>
      <div id="app">
   <app-header></app-header>
    @yield('content')
        <app-footer></app-footer>
        </div>
        <script>
          const app_url = "{{config('app.url')}}";
        </script>
        <script src="{{asset('js/app.js') }}"></script>   
          <!-- Revolution Slider Tools-->
          <script src="{{asset('js/jquery.themepunch.revolution.min.js')}}"></script>
          <!-- Revolution Slider-->
          <script type="text/javascript" src="{{asset('js/revolution.extension.slideanims.min.js')}}"></script>
          <script type="text/javascript" src="{{asset('js/revolution.extension.layeranimation.min.js')}}"></script>
          <script type="text/javascript" src="{{asset('js/revolution.extension.navigation.min.js')}}"></script>
          <script src="{{asset('js/jquery.form.js')}}"></script>
          {{--  <script src="{{asset('js/jquery.validate.min.js')}}"></script>           --}}
          <script src="{{asset('js/jquery-ui.min.js')}}"></script>
          <script src="{{asset('js/imagelightbox.min.js')}}"></script>
          <script src="{{asset('js/owl.carousel.js')}}"></script>

          {{--  <script src="{{asset('js/theme.js') }}"></script>   --}}
    
        </body>
      </html>