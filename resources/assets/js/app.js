
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('jquery-lazy');
require('jquery-validation');

import swal from 'sweetalert';

window.Vue = require('vue');
import VueRouter from 'vue-router';
require('./api');
require('./data');
Vue.use(VueRouter);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
window.bus = new Vue();

const appHeader = Vue.component('app-header',require('./components/HeaderComponent.vue'));
const appFooter = Vue.component('app-footer',require('./components/FooterComponent.vue'));
Vue.component('home-banner',require('./components/HomeBannerComponent.vue'));
Vue.component('home-services',require('./components/HomeServicesComponent.vue'));
Vue.component('home-control-compliance',require('./components/HomeControlCompliance.vue'));
Vue.component('testimonial-slider',require('./components/TestimonialSliderComponent.vue'));
Vue.component('home-about',require('./components/HomeAboutComponent.vue'));
Vue.component('home-projects',require('./components/HomeProjectsComponent.vue'));
Vue.component('loading-component',require('./components/LoadingComponent.vue'));
Vue.component('home-more-products',require('./components/HomeMoreProductsComponent.vue'));
Vue.component('home-contact-form',require('./components/HomeContactFormComponent.vue'));
Vue.component('login-form',require('./components/LoginComponent.vue'));


const homeComponent = Vue.component('home-component',require('./components/HomeComponent.vue'));
const serviceComponent = Vue.component('service-component',require('./components/ServiceDetailsComponent.vue'));
const shopComponent = Vue.component('shop-component',require('./components/ShopComponent'));
const productComponent = Vue.component('product-component',require('./components/ProductDetailsComponent'));
const cartComponent = Vue.component('cart-component',require('./components/CartComponent'));
const checkOutComponent = Vue.component('checkout-component',require('./components/CheckOutComponent'));
const aboutUsComponent = Vue.component('about-component',require('./components/AboutUsComponent'));
const contactComponent = Vue.component('contact-component',require('./components/ContactUsComponent'));
const galleryComponent = Vue.component('gallery-component',require('./components/GalleryComponent'));
const searchComponent = Vue.component('search-component',require('./components/SearchComponent'));
const workHistoryComponent = Vue.component('work-history-component',require('./components/WorkHistoryComponent'));
const pageNotFound = Vue.component('not-found',require('./components/NotFoundComponent'));
const offlineInstallation = Vue.component('offline-installation',require('./components/OfflineInstallation.vue'));


const routes = [
    {path: '/',component:homeComponent,name:'Home'},
    {path:'/service/info/:id/:caption',component:serviceComponent,props:true,name:'Service'},
    {path:'/shop/products',component:shopComponent,props:true,name:'Shop'},
    {path:'/product/details/:id/:name',component:productComponent,props:true,name:'Product'},
    {path: '/cart',component:cartComponent,name:'Cart'},
    {path: '/checkout',component:checkOutComponent,name:'Checkout'},
    {path: '/about',component:aboutUsComponent,name:'About'},
    {path:'/contact',component:contactComponent,name:'Contact'},
    {path:'/gallery',component:galleryComponent,name:'Gallery'},
    {path:'/search',component:searchComponent,name:'Search',props: (route) => ({ query: route.query.q })},
    {path:'/work/history',component:workHistoryComponent,name:'WorkHistory'},
    {path:'/offline-installation',component:offlineInstallation,name:'Offline-Installation'},
    {path: '*',component:pageNotFound}
];

const router = new VueRouter({
    routes,
    scrollBehavior (to, from, savedPosition) {
        if (savedPosition) {
          return savedPosition
        } else {
          return { x: 0, y: 0 }
        }
      }
});

const app = new Vue({
    router
}).$mount('#app');

