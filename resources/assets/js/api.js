

window.appUrl = app_url;
window.adminUrl = 'https://admin.oicsolar.net';

// window.adminUrl = 'http://localhost/admin-oicsolar';


window.servicePoster = adminUrl+'/storage/images/services';
window.productPoster = adminUrl+'/storage/images/products';
window.galleryPoster = adminUrl+'/storage/images/gallery';
window.slidePoster = adminUrl+'/storage/images/slides';


window.saveServiceRequest = appUrl + '/service/request';
window.getServices = appUrl+'/services';
window.getProducts = appUrl+'/shop/products';
window.randomProducts = appUrl+'/shop/products/random/%s';

window.productDetails = appUrl+'/products/%s';

window.cart = appUrl+'/cart';
window.addToCart = appUrl+'/cart/store';
window.removeFromCart = appUrl+'/cart/%s/edit';
window.destroyCart = appUrl+'/cart/destroy';
window.storeCart = appUrl+'/cart/put/total';

window.checkUser = appUrl+'/user/state';

window.storeOrder = appUrl+'/order/store';

window.storeContactForm = appUrl+'/contact/store';

window.getGallery = appUrl+'/gallery';
window.searchApplication = appUrl+'/search';

window.authenticateUser = appUrl+'/login';
window.logOut = appUrl+'/logout';
window.geolocationApi = appUrl+'/api/geolocation';
window.offlineInstallation = appUrl+'/offline-installation';

window.slides = appUrl+'/slides';
window.workhistory_slides = appUrl+'/workhistory-slides';


window.showError = (text) => {
  swal({
    title: "Error",
    text: text,
    icon: "error",
    button: "Okay",
  });
}
window.showWarning = (text) => {
  swal({
    title: "Warning",
    text: text,
    icon: "warning",
    button: "Okay",
  });
}
window.showInfo = (text) => {
  swal({
    title: "Information",
    text: text,
    icon: "info",
    button: "Okay",
  });
}
window.showSuccess = (title, text, button) => {
  swal({
    title: title,
    text: text,
    icon: "success",
    button: "Okay",
  });
}

//Global Confirmation Dialog
window.globalConfirmationDialog = (inputTitle, inputText, successCallback, cancelCallback) => {
  swal({
    title: inputTitle,
    text: inputText,
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
    .then((willDelete) => {
      if (willDelete) {
        successCallback()
      } else {
        if (cancelCallback != null) {
          cancelCallback();
        }
      }
    });
}