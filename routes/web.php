<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/test', 'HomeController@test');


Route::get('/shop/products', 'ShopController@index');
Route::get('/shop/products/random/{count}', 'ShopController@random');

Route::post('/service/request', 'ServiceRequestController@saveServiceRequest');

Route::get('/services', 'ServiceController@services');

Route::post('/cart/store', 'CartController@store');
Route::get('/cart/destroy', 'CartController@destroy');

Route::post('/cart/put/total', 'CartController@storeWholeCart');
Route::resource('/cart', 'CartController');

Route::resource('/products', 'ProductController');

Route::get('/user/state', 'Auth\LoginController@checkUserState');

Route::post('/order/store', 'OrderController@store');

Route::post('/contact/store', 'ContactController@store');

Route::get('/gallery', 'GalleryController@index');

Route::get('/search', 'SearchController@search');

Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout');

Route::resource('/offline-installation', 'OfflineInstallationController');
Route::get('/slides', 'HomePageSlideController@index');
Route::get('/workhistory-slides', 'WorkHistorySlidesController@index');