<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomepageSlides extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('homepage_slides', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('header',70);
            $table->string('text',150);
            $table->string('background_image',255);
            $table->string('link_route_name_a',50);
            $table->string('link_text_a',50);
            $table->string('link_route_name_b',50);
            $table->string('link_text_b',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homepage_slides');
    }
}
