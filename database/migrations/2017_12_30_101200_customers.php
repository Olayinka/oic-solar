<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Customers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (BluePrint $table) {
            $table->integer('id', true);
            $table->string('first_name', 191);
            $table->string('last_name', 191);
            $table->string('email')->unique();
            $table->string('password');
            $table->string('address');
            $table->string('more_address')->nullable();
            $table->string('town');
            $table->string('phone_number');
            $table->string('country');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
