<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Gallery;
use App\Models\Product;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        //retrieve product
        $products = Product::where('product_name', 'like', '%'.$request->input('q').'%')->orWhere('product_description', 'like', '%'.$request->input('q').'%')->paginate(10);
        $gallery = Gallery::where('caption', 'like', '%'.$request->q.'%')->paginate(10);
        return response()->json(array("products"=>$products,"gallery"=>$gallery));
    }
}
