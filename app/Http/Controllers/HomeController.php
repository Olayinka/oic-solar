<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cart;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome');
    }
    public function test()
    {
        $cart = Cart::where('customer_id', 1)->get();
        foreach ($cart as $item) {
            $tempProduct = $item->product;
            $tempProduct["quantity_cart"] = $item->quantity_cart;
            $products[] = $tempProduct;
        }
        return response()->json($products);
    }
}
