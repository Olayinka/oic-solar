<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Customer;
use App\Models\Cart;
use App\Models\Order;
use Log;

class OrderController extends Controller
{
    public function checkUserState()
    {
        if (Auth::check()) {
            return response()->json(array('message'=>'user is logged in, retrieve cart from db','userAuthenticated'=>true));
        } else {
            return response()->json(array('message'=>'Anonymous User','userAuthenticated'=>false));
        }
    }
    public function store(Request $request)
    {
        Log::info($request->first_name);
        //hold cart info
        $cart = $request->session()->get('cart', array());
        if (!Auth::check()) {
            if ($request->session()->has('cart')) {
                $checkCustomer = Customer::where('email', $request->email)->get();
                if ($checkCustomer->count()>0) {
                    Log::info('customer exist');
                    foreach ($cart as $product) {
                        $order = new Order;
                        $order->product_id = $product["id"];
                        $order->note = $request->note ? $request->note :  '';
                        $order->customer_id = $checkCustomer[0]->id;
                        $order->quantity = $product["quantity_cart"];
                        $order->save();
                    }
                } else {
                    Log::info('customer does not exist');
                    //save customer to db
                    $customer["email"] = $request->email;
                    $customer["first_name"] = $request->first_name;
                    $customer["last_name"] = $request->last_name;
                    $customer["password"] = bcrypt($request->password);
                    $customer["town"] = $request->town;
                    $customer["country"] = $request->country;
                    $customer["phone_number"] = $request->phone;
                    $customer["address"] = $request->address;
                    $customer["more_address"] = $request->moreAddress ? $request->moreAddress : '';
                    $newCustomer = Customer::create($customer);
                    foreach ($cart as $product) {
                        $order = new Order;
                        $order->product_id = $product["id"];
                        $order->note = $request->note ? $request->note :  '';
                        $order->customer_id = $newCustomer->id;
                        $order->quantity = $product["quantity_cart"];
                        $order->save();
                    }
                }
            } else {
                return response()->json(array('action'=>false,'message'=>'Your cart is empty. You can check our shop for more products'));
            }
        } else {
            $cartFromDb = Cart::where('customer_id', Auth::user()->id)->get();
            foreach ($cartFromDb as $product) {
                $order = new Order;
                $order->product_id = $product->product_id;
                $order->note = $request->note ? $request->note :  '';
                $order->customer_id = $request->user()->id;
                $order->quantity = $product->quantity_cart;
                $order->save();
            }
        }
        $request->session()->forget('cart');
        if (Auth::check()) {
            Cart::where('customer_id', Auth::user()->id)->delete();
        }
        return response()->json(array('action'=>true,'message'=>'Order submitted successfully. You can check our shop for more products'));
    }
}
