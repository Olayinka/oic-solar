<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HomePageSlide;

class HomePageSlideController extends Controller
{
    public function index(Request $request){
        $slides = HomePageSlide::all();
        return response()->json($slides);
    }
}
