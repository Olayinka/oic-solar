<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OfflineInstallation;
use Illuminate\Support\Facades\Auth;
use App\Models\Customer;
use Log;

class OfflineInstallationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::check()) {
            $checkCustomer = Customer::where('email', $request->email)->get();
            if ($checkCustomer->count()>0) {
                Log::info('customer exist');
                $customer = $checkCustomer->first();
                $order = new OfflineInstallation();
                $order->first_name = $customer->first_name;
                $order->last_name = $customer->last_name;
                $order->email = $customer->email;
                $order->address = $request->address;
                $order->note = $request->note;
                $order->state = $request->state;
                $order->town = $request->town;
                $order->phone_number = $request->phone;
                $order->country = $request->country;
                $order->save();
            } else {
                Log::info('customer does not exist');
                //save installatioin to db
                $order = new OfflineInstallation();
                $order->first_name = $request->first_name;
                $order->last_name = $request->last_name;
                $order->email = $request->email;
                $order->address = $request->address;
                $order->note = $request->note;
                $order->state = $request->state;
                $order->town = $request->town;
                $order->phone_number = $request->phone;
                $order->country = $request->country;
                $order->save();
            }
        } else {
                $email = $request->user->email;
                $customer = Customer::where('email', $email)->get()->first();
                $order = new OfflineInstallation();
                $order->first_name = $customer->first_name;
                $order->last_name = $customer->last_name;
                $order->email = $customer->email;
                $order->address = $request->address;
                $order->note = $request->note;
                $order->state = $request->state;
                $order->town = $request->town;
                $order->phone_number = $request->phone;
                $order->country = $request->country;
                $order->save();
        }
        return response()->json(array('action'=>true,'message'=>'Order submitted successfully. You will be contacted shortly'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
