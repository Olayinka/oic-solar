<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ServiceRequest;
use Log;

class ServiceRequestController extends Controller
{
    public function saveServiceRequest(Request $request)
    {
        if ($request->has('name')&& $request->has('email') && $request->has('phone') && $request->has('message') && $request->has('service')) {
            $serviceRequest = new ServiceRequest;
            $serviceRequest->name = $request->name;
            $serviceRequest->email = $request->email;
            $serviceRequest->phone_number = $request->phone;
            $serviceRequest->message = $request->message;
            $serviceRequest->service = $request->service;
            $serviceRequest->save();
            return response()->json(array('action'=>true,'validation'=>true,'message'=>'Your service request was submitted succesfully. Our support team will contact you shortly'));
        } else {
            return response()->json(array('action'=>false,'validation'=>false,'message'=>'Please provide name, email, phone, message and service'));
        }
    }
}
