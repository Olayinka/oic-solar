<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Cart;
use Log;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['logout','checkUserState']);
    }
    public function login(Request $request)
    {
        $cart =  $request->session()->get('cart', array());
        if (!Auth::check()) {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                //store cart in db
                foreach ($cart as $item) {
                    $check = Cart::where('customer_id', Auth::user()->id)->where('product_id', $item["id"]);
                    if ($check->count()==0) {
                        $cart = new Cart;
                        $cart->customer_id = Auth::user()->id;
                        $cart->product_id = $item["id"];
                        $cart->quantity_cart = $item["quantity_cart"];
                        $cart->save();
                    }
                }
                //retrieve cart
                $cartFromDb = Cart::where('customer_id', Auth::user()->id)->get();
                foreach ($cartFromDb as $item) {
                    $tempProduct = $item->product;
                    $tempProduct["quantity_cart"] = $item->quantity_cart;
                    $products[] = $tempProduct;
                }
                if (is_null($products)) {
                    $products = array();
                }

                return response()->json(array("cart"=>$products,"messsage"=>"User authenticated successfully","authenticated"=>true,"user"=>Auth::user()));
            } else {
                return response()->json(array("message"=>'Incorrect username or password','authenticated'=>false));
            }
        } else {
            //retrieve cart
            $cart = Cart::where('customer_id', Auth::user()->id)->get();
            foreach ($cart as $item) {
                $tempProduct = $item->product;
                $tempProduct["quantity_cart"] = $item->quantity_cart;
                $products[] = $tempProduct;
            }
            if (is_null($products)) {
                $products = array();
            }
            return response()->json(array("cart"=>$products,"messsage"=>"User already logged in","authenticated"=>true,"user"=>Auth::user()));
        }
    }
    public function checkUserState()
    {
        if (!Auth::check()) {
            return response()->json(array("message"=>'Anonymous','authenticated'=>false));
        } else {
            return response()->json(array("messsage"=>"User already logged in","authenticated"=>true,"user"=>Auth::user()));
        }
    }
    public function logout(Request $request)
    {
        $cart =  $request->session()->get('cart', array());
        Auth::logout();
        $request->session()->put('cart', $cart);
        return response()->json(array("message"=>'Anonymous','action'=>true));
    }
}
