<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Log;

class ShopController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::paginate(15);
        return response()->json($products);
    }
    public function random($count)
    {
        $products = Product::inRandomOrder()->limit($count)->get();
        return response()->json($products);
    }
}
