<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;

header("Access-Control-Allow-Origin: *");

class GeolocationController extends Controller
{
    public function get(Request $request)
    {
        $data = file_get_contents('https://api.ipgeolocation.io/ipgeo?apiKey=0ada2f889e844a4aa1d709a049e470ef');
        return response()->json(json_decode($data));
    }
}
