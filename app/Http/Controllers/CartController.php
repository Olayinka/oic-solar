<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Cart;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Auth::check()) {
            $cart = Cart::where('customer_id', Auth::user()->id)->get();
            foreach ($cart as $item) {
                $tempProduct = $item->product;
                $tempProduct["quantity_cart"] = $item->quantity_cart;
                $products[] = $tempProduct;
            }
            if (isset($products)) {
                return response()->json($products);
            } else {
                return response()->json(array());
            }
        } else {
            return response()->json($request->session()->get('cart', array()));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::check()) {
            if ($request->session()->has('cart')) {
                $carts =   $request->session()->get('cart', array());
                $productExist = false;
                foreach ($carts as $cart) {
                    if ($cart["id"]==$request->product["id"]) {
                        $productExist = true;
                    }
                }
                if (!$productExist) {
                    $request->session()->push('cart', $request->product);
                }
            } else {
                $request->session()->put('cart', array($request->product));
            }
        } else {
            $checkCart = Cart::where('customer_id', Auth::user()->id)->where('product_id', $request->product["id"]);
            if ($checkCart->count()==0) {
                $cart = new Cart;
                $cart->customer_id = Auth::user()->id;
                $cart->product_id = $request->product["id"];
                $cart->quantity_cart = $request->product["quantity_cart"];
                $cart->save();
            }
        }
        return 'Product has been added to cart';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if (!Auth::user()) {
            $carts =   $request->session()->get('cart', array());
            foreach ($carts as $cart) {
                if ($cart["id"]!=$id) {
                    $newCart[] = $cart;
                }
            }
            $request->session()->forget('cart');
            if (isset($newCart)) {
                $request->session()->put('cart', $newCart);
            }
        } else {
            Cart::where('product_id', $id)->where('customer_id', Auth::user()->id)->delete();
        }
        return response()->json(array("message"=>"product removed successfully"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function storeWholeCart(Request $request)
    {
        if (!Auth::check()) {
            $request->session()->put('cart', $request->cart);
        } else {
            Cart::where('customer_id', Auth::user()->id)->delete();
            foreach ($request->cart as $item) {
                $cart = new Cart;
                $cart->customer_id = Auth::user()->id;
                $cart->product_id = $item["id"];
                $cart->quantity_cart = $item["quantity_cart"];
                $cart->save();
            }
        }
        return response()->json(array("message"=>"cart stored successfully"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if (!Auth::check()) {
            $request->session()->forget('cart');
        } else {
            Cart::where('customer_id', Auth::user()->id)->delete();
        }
    }
}
