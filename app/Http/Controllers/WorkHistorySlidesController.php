<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\WorkHistorySlide;

class WorkHistorySlidesController extends Controller
{
    public function index(Request $request){
        $slides = WorkHistorySlide::all();
        return response()->json($slides);
    }
}
