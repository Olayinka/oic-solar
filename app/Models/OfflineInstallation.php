<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfflineInstallation extends Model
{
    protected $table = 'offline_installations';
}
